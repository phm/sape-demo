_Readme de démonstration_

## Savoirs à enseigner pour enseigner

> [Université de Lille](http://univ-lille.fr)  
> [Licence d'informatique](http://portail.fil.univ-lille1.fr/ls4)  
> Parcours métiers de l'enseignement

> Philippe Marquet, philippe.marquet@univ-lille.fr

Les supports de présentation sont sur le NextCloud 

* https://nextcloud.univ-lille.fr/index.php/s/N2G9GPADmj6Z2xr
* voir le `Readme.md` de ce dépôt... au format Markdown !

### 1- Didactique

* support de présentation `sape-1.pdf`

### 2- Informatique sans ordinateur

* activités d'informatique sans ordinateur
    - crépier pyschorigide,  
	  magie binaire,  
	  code de la télé-vision,  
	  gobot,  
	  M-10 la machine débranchée,   
	  pesée qui trie  
* support de présentation `2021-03-sape-2-a.pdf`
* support de présentation `2021-03-sape-2-c.pdf`

### 3- Outil``

_à venir_ 



